package com.example.titi.myapplication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.view.View;

public class BtClientConnection extends Thread {

    private static final String LOG_TAG = "BB8 : BtClientConn";

    private final BluetoothSocket mSocket;
    private final BluetoothDevice mDevice;

    private int readBufferPosition;
    private byte[] readBuffer;
    private Thread workerThread;
    private boolean stopWorker;

    private final OutputStream mOutput;
    private final InputStream mInput;


    public enum State{CONNECT, CONNECTED, DISCONNECT};
    public State mState;

    public BtClientConnection(BluetoothDevice device) {
        //Log.i(LOG_TAG, "progress visible ");
        //Connection.progressBarBT.setVisibility(View.VISIBLE);
        //Connection.lblResult.setText("Connexion à "+ device.getName());
        //Connection.lblResult.setTextColor(Color.BLACK);
        mDevice = device;
        BluetoothSocket socket = null;
        OutputStream out = null;
        InputStream in = null;


        try {
            socket = mDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            out = socket.getOutputStream();
            in = socket.getInputStream();
        } catch (Exception e) {
            Log.e(LOG_TAG, "error while creating BT socket ");
            e.printStackTrace();
        }

        mSocket = socket;
        mOutput = out;
        mInput = in;

        //beginListenForData();

        mState = State.CONNECT;


        //Log.e(LOG_TAG, "progress gone ");
        //Connection.progressBarBT.setVisibility(View.GONE);
    }

    void beginListenForData(){
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character
        Log.e(LOG_TAG, "BT RECEIVER beginListenForData()");

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable()
        {
            public void run()
            {
                Log.e(LOG_TAG, "BT RECEIVER run()");
                while(!Thread.currentThread().isInterrupted() && !stopWorker)
                {
                    try
                    {
                        int bytesAvailable = 0;
                        if(mInput == null) {
                            //mInput = mSocket.getInputStream();
                            Log.e(LOG_TAG, "BT Socket NULL");
                        }
                        //else if (mInput != null){
                        //    bytesAvailable = mSocket.getInputStream().available();
                        //}

                        if(bytesAvailable > 0)
                        {
                            Log.e(LOG_TAG, "BT RECEIVE "+ bytesAvailable + " bytes");

                            byte[] packetBytes = new byte[bytesAvailable];
                            mInput.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++)
                            {
                                byte b = packetBytes[i];
                                if(b == delimiter)
                                {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable()
                                    {
                                        public void run()
                                        {
                                            Log.e(LOG_TAG, "BT RECEIVER RUN " +data);
                                        }
                                    });
                                }
                                else
                                {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    catch (IOException ex)
                    {
                        Log.e(LOG_TAG, "BT RECEIVER exception : "+ex.toString());
                        stopWorker = true;
                    }
                }
            }
        });

        workerThread.start();
    }

    public void run() {
        Log.e(LOG_TAG, "Client start");
        if (mSocket == null) return;

        Log.e(LOG_TAG, "BT RECEIVER beginListenForData launching ");
        beginListenForData();
        Log.e(LOG_TAG, "BT RECEIVER beginListenForData launched : ");

        while (true) {
            switch (mState) {
                case CONNECT:
                    try {
                        mSocket.connect();
                    } catch (IOException e) {
                        e.printStackTrace();
                        mState = State.DISCONNECT;
                        return;
                    }
                    mState = State.CONNECTED;
                    Log.d(LOG_TAG, "Connected!");
                    break;
                case CONNECTED:

                    break;
                case DISCONNECT:
                    if (mSocket != null) {
                        try {
                            mOutput.write('E');
                            TimeUnit.MILLISECONDS.sleep(100);
                            mSocket.close();
                            Log.d(LOG_TAG, "Disconnected");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return;
            }
        }
    }

    public void send(byte sendData[]) {

        if (!mState.equals(State.CONNECTED)) return;
        try {
            if(sendData.length == 1)
                Log.i(LOG_TAG, "BT : send "+ sendData[0]);
            if(sendData.length ==4 || sendData.length ==2)
                Log.i(LOG_TAG, "BT : send "+ (byte)sendData[0] + ", " + (byte)sendData[1]);
            mOutput.write(sendData);
        } catch (Exception e) {
            Log.i(LOG_TAG, "BT : send FAILED "+ sendData[0]);
            e.printStackTrace();
        }
    }

    public void disconnect() {
        Log.i(LOG_TAG, "BT device disconected as requested by the user");
        mState  = State.DISCONNECT;
    }

}