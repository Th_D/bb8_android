package com.example.titi.myapplication;

import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.util.Log;

public class BarUpdateBT extends CountDownTimer {

    public BarUpdateBT(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    @Override
    public void onTick(long millisUntilFinished) {
        Connection.btnSearch.setClickable(false);
        Connection.btnSearch.setEnabled(false);
        int progress = 110 - (int) (millisUntilFinished/100);
        Connection.progressBarBT.setProgress(progress);
    }

    @Override
    public void onFinish() {
        Connection.progressBarBT.setProgress(0);
        Connection.btnSearch.setEnabled(true);
        Connection.btnSearch.setClickable(true);
    }
}