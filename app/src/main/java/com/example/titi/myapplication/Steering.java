package com.example.titi.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class Steering extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener{
    private float centerX;
    private float centerY;
    private float baseRadius;
    private float hatRadius;
    private float widthX;
    private float heightY;
    final int scaleXY = 100;
    private long timeLstSnd = 0;


    private JoystickListener joystickCallback = new JoystickListener() {
        @Override
        public void onJoystickMoved(float percentX, float percentY, int id) {

        }
    };
    public final int ratio = 5;

    private void setupDimensions(){
        centerX = getWidth() /2;
        centerY = getHeight() /2;
        widthX = getWidth();
        heightY = getHeight();

        baseRadius = Math.min(getWidth(), getHeight()) /3;
        hatRadius = Math.min(getWidth(), getHeight()) /5;
    }

    public Steering(Context context){
        super(context);
        getHolder().addCallback(this);
        setOnTouchListener(this);
        if(context instanceof JoystickListener){
            joystickCallback = (JoystickListener) context;
        }
    }

    private void drawJoystick(float newX, float newY){
        int realX, realY;
        int speed;
        realX = (int) ((newX - centerX) / baseRadius * scaleXY);
        realY = (int) ((-(newY - centerY) ) / baseRadius * scaleXY);
        speed = (int) Math.sqrt((Math.pow(realX, 2)) + Math.pow(realY, 2));

        Log.e("XY",  realX + " "+ realY + "S= "+speed);

        if(getHolder().getSurface().isValid()){
            Canvas myCanvas = this.getHolder().lockCanvas();
            Paint colors = new Paint();
            myCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            colors.setARGB(255, 255, 255, 255); //base du joystock en blanc
            myCanvas.drawCircle(centerX, centerY, baseRadius, colors);
            myCanvas.drawCircle(centerX, centerY-380, baseRadius/2, colors);

            colors.setARGB(200, 71, 71, 71); //gris
            myCanvas.drawCircle(centerX-30, centerY-450, baseRadius/8, colors);
            myCanvas.drawCircle(centerX+50, centerY-380, baseRadius/11, colors);
            //myCanvas.drawRect(centerX-80, centerY-500, centerX+80, centerY-490, colors);

            colors.setARGB(255, 255, 255, 255); //blanc
            myCanvas.drawCircle(centerX+50, centerY-380, baseRadius/15, colors);colors.setARGB(255, 255, 255, 255);

            colors.setARGB(200, 71, 71, 71);
            myCanvas.drawCircle(centerX+50, centerY-380, baseRadius/17, colors);


            colors.setARGB(255, 255, 102, 0); //joystick en orange
            //myCanvas.drawRect(centerX-85, 500, centerX+85, 510, colors);
            myCanvas.drawCircle(newX, newY, hatRadius, colors);
            myCanvas.drawRect(centerX-15, centerY-baseRadius, centerX+15, centerY-200, colors); //haut
            myCanvas.drawRect(centerX-15, centerY+200, centerX+15, centerY+baseRadius, colors); //bas
            myCanvas.drawRect(centerX-baseRadius, centerX+200, centerX+200, centerX+230, colors); //droite
            //myCanvas.drawRect(centerY-200, centerX+180, centerY-420, centerX+baseRadius, colors); //gauche

            colors.setARGB(255, 255, 255, 255); //joystick en orange
            myCanvas.drawCircle(newX, newY, hatRadius-50, colors);

            colors.setARGB(255, 255, 102, 1); //application name en orange
            colors.setTextSize(100);
            myCanvas.drawText("BB8", centerX-95, centerY+30, colors);
            colors.setARGB(255, 255, 255, 255); //names en blanc
            colors.setTextSize(60);
            myCanvas.drawText("Groupe C2", 80, 1350, colors);
            colors.setTextSize(40);
            myCanvas.drawText("Adel BEN KHEMIS", centerX+20, 1300, colors);
            myCanvas.drawText("Asmâ ZIYANI", centerX+20, 1340, colors);
            myCanvas.drawText("Thibault DELORME", centerX+20, 1380, colors);
            myCanvas.drawText("Quentin MASSACRIER", centerX+20, 1420, colors);
            myCanvas.drawText("Pauline VALLET", centerX+20, 1460, colors);

            colors.setTextSize(40);
            myCanvas.drawText(realX + " "+ realY + " S= " + speed, 40, 40, colors);
            getHolder().unlockCanvasAndPost(myCanvas);
            Log.e("XY",  "BB8 TIME  =" + String.valueOf(System.nanoTime()-timeLstSnd));
            if( (timeLstSnd == 0) || (System.nanoTime() - timeLstSnd > 100 * 1000000) || ((realX / 2 + 100 == 100) && (realY / 2 + 100 == 100))) { // *1000000 to get millisec
                timeLstSnd = System.nanoTime();
                Log.e("XY",  "BB8 X=" + String.valueOf(realX / 2 + 100) + "  Y=" + String.valueOf(realY / 2 + 100));
                if ((realX / 2 + 100 == 100) && (realY / 2 + 100 == 100)) { //send 2 times if (0,0) to double check
                    Connection.mConnection.send(new byte[]{
                            (byte) (100), (byte) (100)
                    });
                    Connection.mConnection.send(new byte[]{
                            (byte) (100), (byte) (100)
                    });
                } else {
                    Connection.mConnection.send(new byte[]{
                            (byte) (realX / 2 + 100), (byte) (realY / 2 + 100)
                    });
                }
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder){
        setupDimensions();
        drawJoystick(centerX,centerY);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder){

    }

    public boolean onTouch(View view, MotionEvent event){
        if(view.equals(this)){
            if(event.getAction() != event.ACTION_UP){
                float displacement = (float) Math.sqrt((Math.pow(event.getX() - centerX, 2)) + Math.pow(event.getY() - centerY, 2));
                if(displacement<baseRadius){
                    drawJoystick(event.getX(), event.getY());
                    joystickCallback.onJoystickMoved((event.getX() - centerX)/baseRadius, (event.getY() - centerY)/baseRadius, getId());
                }else{
                    float ratio = baseRadius / displacement;
                    float dX = centerX + (event.getX() - centerX) * ratio;
                    float dY = centerY + (event.getY() - centerY) * ratio;
//                    Log.e("XY",  dX + " "+ dY);
                    drawJoystick(dX, dY);
                    joystickCallback.onJoystickMoved((dX-centerX)/baseRadius, (dY-centerY)/baseRadius, getId());
                }
            }else{
                drawJoystick(centerX, centerY);
                joystickCallback.onJoystickMoved(0,0,getId());
            }
            return true;
        }
        return false;
    }

    public interface JoystickListener{
        void onJoystickMoved(float percentX, float percentY, int id);
    }


}
