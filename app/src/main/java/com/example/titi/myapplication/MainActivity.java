package com.example.titi.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "BB8 : MainActivity";

    public static boolean connected = false;
    //public static String connectionName = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(LOG_TAG,"MainActivity :: onCreated ()");
        if (!connected){
            Intent myIntent = new Intent(this, Connection.class);
            startActivity(myIntent);
            //setContentView(R.layout.connection);
            Log.i(LOG_TAG, "Pas de BB8 connecté -> affichage de la page de co");
        }
        else{
            setContentView(R.layout.steering);
            Log.i(LOG_TAG, "BB8 connecté -> affichage de la page de pilotage");
        }
    }
}
