package com.example.titi.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Connected extends AppCompatActivity {

    private static final String LOG_TAG = "BB8 : Connected";

    Button btnDeco;
    Button btnPilot;
    Connected actualContext = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connected);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnDeco = (Button) findViewById(R.id.btnDeco);
        btnDeco.setOnClickListener(decoClickListener);

        btnPilot = (Button) findViewById(R.id.btnPilot);
        btnPilot.setOnClickListener(pilotClickListener);
    }

    private View.OnClickListener decoClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.i(LOG_TAG, "click on deco btn");
            Intent myIntentConnected = new Intent(getBaseContext(),Connection.class); //getBaseContext ?
            Connection.mConnection.disconnect();
            startActivity(myIntentConnected);
        }
    };

    private View.OnClickListener pilotClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.i(LOG_TAG, "click on pilot btn");
            Steering joystick = new Steering(actualContext);
            setContentView(joystick);
        }
    };

}
